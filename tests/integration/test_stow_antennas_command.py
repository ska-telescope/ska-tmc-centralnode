"""Test cases for stow antennas command"""
import time

import pytest
from ska_tango_base.commands import ResultCode
from ska_tmc_common.dev_factory import DevFactory

from ska_tmc_centralnode.utils.constants import CENTRALNODE_MID
from tests.integration.conftest import ensure_checked_devices
from tests.settings import SLEEP_TIME, TIMEOUT, logger


@pytest.mark.skip(
    reason="Test needs update as per v0.13. Can be done as a part of \
        further commands refactoring."
)
@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_stow_antennas_command(tango_context):
    """Tests stow antennas command"""
    logger.info("%s", tango_context)
    dev_factory = DevFactory()
    central_node = dev_factory.get_device(CENTRALNODE_MID)
    ensure_checked_devices(central_node)
    initial_len = len(central_node.commandExecuted)
    (result, unique_id) = central_node.On()
    (result, unique_id) = central_node.StowAntennas(["1"])
    assert result[0] == ResultCode.QUEUED
    start_time = time.time()
    while len(central_node.commandExecuted) != initial_len + 2:
        time.sleep(SLEEP_TIME)
        elapsed_time = time.time() - start_time
        if elapsed_time > TIMEOUT:
            pytest.fail("Timeout occurred while executing the test")

    for command in central_node.commandExecuted:
        if command[0] == unique_id[0]:
            logger.info("command result: %s", command)
            assert command[2] == "ResultCode.OK"
